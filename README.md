Numbers Web App
====

Simple Java web app that compute some common
numeric functions with arbitrary precision (aka big-nums).

Build
----

    gradle build

Execute Locally
----

    gradle jettyRunWar

Open a browser to [http://localhost:9000/numbers](http://localhost:9000/numbers)

Deployment
----

Copy the WAR file into a servlet container like Tomcat

    ./build/libs/*.war


