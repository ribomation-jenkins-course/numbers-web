package ribomation.numbers.func;

import java.math.BigInteger;

/**
 * Computes the sum(1..n) using Gauss' summation formula: n(n+1)/2.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Triangular_number">Triangular Numbers @ WikiPedia</a>
 */
public class Sum extends CachingFunction {
    @Override
    protected BigInteger computeResult(Integer arg) {
        BigInteger n   = BigInteger.valueOf(arg);
        BigInteger n1  = n.add(BigInteger.ONE);
        BigInteger two = BigInteger.valueOf(2);
        return n.multiply(n1).divide(two);
    }
}
