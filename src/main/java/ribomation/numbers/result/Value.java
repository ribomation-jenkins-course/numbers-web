package ribomation.numbers.result;

//import org.simpleframework.xml.Element;
//import org.simpleframework.xml.Root;

import java.math.BigInteger;

/**
 * Encapsulates a single timed function value
 *
 * @user jens
 * @date 2015-05-03
 */
//@Root
public class Value {
    public String     name;
    
    //@Element
    public BigInteger result;
    
    //@Element
    public double     elapsedMilliSecs;

    public Value(String name, BigInteger result, double elapsedMilliSecs) {
        this.name = name;
        this.result = result;
        this.elapsedMilliSecs = elapsedMilliSecs;
    }

    public String getName() {
        return name;
    }

    public BigInteger getResult() {
        return result;
    }

    public double getElapsedMilliSecs() {
        return elapsedMilliSecs;
    }

    @Override
    public String toString() {
        return "{" +
                result +
                ", elapsed=" + elapsedMilliSecs + " ms" +
                '}';
    }
}
