package ribomation.numbers.result;

import ribomation.numbers.func.Factorial;
import ribomation.numbers.func.Fibonacci;
import ribomation.numbers.func.Function;
import ribomation.numbers.func.Sum;

import java.util.ArrayList;
import java.util.List;

/**
 * Logical model for this library.
 *
 * @user jens
 * @date 2015-05-03
 */
public class Model {
    private List<Function> functions = new ArrayList<>();
    private List<Result>   results   = new ArrayList<>();

    public Model(List<Function> functions) {
        this.functions = functions;
    }

    public Model() {
        functions.add(new Sum());
        functions.add(new Fibonacci());
        functions.add(new Factorial());
    }

    public Result compute(Integer argument) {
        Result result = new Result().compute(argument, functions);
        results.add(result);
        return result;
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public List<Result> getResults() {
        return results;
    }
    
    public void clear() {
        results.clear();
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(1000);
        buf.append("Functions: ").append(functions).append("\n");
        for (Result result : results) {
            buf.append(result).append("\n");
        }
        return buf.toString();
    }
}
